//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: jcal.java,v 1.2 2013-08-26 23:05:32-07 - - $
/*
 *
 *NAME
 *   jcal - mimics the output of the cal(1) utility
 *
 *SYNOPSIS
 *   jcal [-local] [[month] year]
 *
 *DESCRIPTION - prints the calendar for any particular 
 *      month or year, mimicing the output of the
 *      cal(1) utility. It also supports output in
 *      anyone one of the ISO-639 language names.
 *
 *
 *
 */
import java.util.GregorianCalendar;
import java.util.Locale;
import static java.lang.System.*;
import static java.util.Calendar.*;
class jcal {
   static final int MONTHS_IN_YEAR = 12;
   static final int WEEKS_IN_MONTH =  6;
   static final int DAYS_IN_WEEK   =  7;
   static final int FIELD_MONTH = GregorianCalendar.MONTH;
   static final int FIELD_YEAR = GregorianCalendar.YEAR;
   static final int LONG = GregorianCalendar.LONG;
   static final GregorianCalendar CHANGE =
      new GregorianCalendar(1752, SEPTEMBER, 14); 
   static Locale LOCAL = Locale.getDefault();
   GregorianCalendar temp = new GregorianCalendar();
   int calmonth, calyear;
   boolean yearmonth;
   /* Basic constructor used to initialize  
   * GregorianCalendar object temp to a pure
   * Gregorian calendar.
   */
   jcal(){
      temp.setGregorianChange(CHANGE.getTime());
   }
   /*
   * Takes parameters month and year, and constructs a two
   * dimension array (6 X 7). The correct month dates are
   * assigned to the corresponding array indexes. A while loop 
   * is used to iterate over each day of the given month and 
   * year. The incrementing is done by the add() method of the
   * GregorianCalendar class.
   */
   int[][] make_month (int month, int year){
    int[][] calmonth = new int[WEEKS_IN_MONTH][DAYS_IN_WEEK];
    temp.set(FIELD_YEAR, year);
    temp.set(FIELD_MONTH, month);
    temp.set(GregorianCalendar.DAY_OF_MONTH, 1);
    while(month == temp.get(FIELD_MONTH)){
      int calday = temp.get (GregorianCalendar.DAY_OF_MONTH);  
      int weekday = temp.get(GregorianCalendar.DAY_OF_WEEK) - 1;
      int weeknum = temp.get(GregorianCalendar.WEEK_OF_MONTH) - 1;
      calmonth[weeknum][weekday] = calday;
      temp.add(GregorianCalendar.DAY_OF_MONTH,1);
    }
    temp.set(FIELD_YEAR, year);
    temp.set(FIELD_MONTH, month);
    return calmonth;
   }

   /*
   * Takes the year parameter, and constructs a three
   * dimension array (12 X 6 X 7). The correct dates are 
   * assigned to the corresponding array indexes. The method
   * utilizes the previous make_month method to construct
   * two dimensional arrays for each month.
   */
   int[][][] make_year (int year){
    int months = MONTHS_IN_YEAR;
    int weeks = WEEKS_IN_MONTH;
    int days = DAYS_IN_WEEK;
    int[][][] calyear = new int[months][weeks][days];
    for(int vari = 0; vari < 12; vari++){
      calyear[vari] = make_month(vari, year);
    }
    temp.set(FIELD_YEAR, this.calyear);
    temp.set(FIELD_MONTH, this.calmonth);
    return calyear;
   }
   
   /*
   * Takes a two dimensional array representing a month.
   * The month name is centered in a space of 20 chars by
   * using the String.format() method. Finally it replaces
   * all occurrences of " 0" with "  ", because cal(1) outputs
   * empty spaces instead of 0's. Returns the resulting string.
   */
   String month_string (int[][] month){
    String print = temp.getDisplayName(FIELD_MONTH, LONG, LOCAL);
    print += (yearmonth)?" " + calyear :"" ;
    int pad = 10 + (print.length()/2); //algorithm: for centering
    String padding = "%-" + pad + "s";
    print = String.format(padding,print);
    print = String.format("%20s",print) + "%n";
    print += "Su Mo Tu We Th Fr Sa%n";
    for (int[] week: month) {
      for (int day = 0; day < week.length; ++day) {
       String format = day == 0? "%2d" : "%3d";
       String date = String.format(format, week[day]);
       print += date;
      }
      print = print.replace(" 0","  ") + "%n";
    }
    return print;
   }

   /*
   * Takes a two dimensional array representing a month.
   * Regions with spaces of 20 chars, are removed because
   * cal(1) does not have such regions of spaces. The last
   * two new lines are eliminated, by shortening the result
   * of the month_string() method by 4 chars. The remaining
   * spaces on the last line of a month till the last date 
   * output is eliminated and replaced with a new line, or
   * two newlines for shorter months. This ensures that only
   * 8 new lines will be outputted which is what cal(1) does.
   */  
   void print_month(int[][] month){
    String spaces = String.format("%20s"," ");
    String printout = month_string(month).replace(spaces,"");
    int k = printout.length() - 4;
    // eliminates spaces at the end of month_string
    for(; printout.substring(k-1,k).equals(" ");k--);
    printout = printout.substring(0,k);
    int count = printout.split("%n").length;
    String newlines = "";
    newlines += (count == 8)?"%n":"%n%n";
    printout = printout + newlines;
    System.out.printf(printout);
   }
   
   /* Takes three dimensional array represneting a year.
   * The year is displayed centered on the first line
   * followed by two new lines. The first for loop iterates
   * every three months, while the second splits the output of 
   * month_string() by newlines. The last for loop assigns the 
   * spacing of months, and where to append a new line. Finally
   * the outermost for loop prints the corresponding row of three 
   * months.
   */
   void print_year(int[][][] year){
    String yr = Integer.toString(calyear);
    int pad = 34 + (yr.length()/2); //algorithm for centering year
    String padding = "%-" + pad + "s";
    yr = String.format(padding,yr);
    yr = String.format("%67s",yr).substring(0,66);
    System.out.print(yr + "\n" + "\n");
    for(int i = 0; i < 12; i += 3){
      temp.set(FIELD_MONTH, i);
      String[] print = (this.month_string(year[i])).split("%n");
      for(int j = i + 1; j < i+3; j++){
       temp.set(FIELD_MONTH, j);   
       String month = this.month_string(year[j]);
       String[] line = month.split("%n");
       for(int l = 0; l < line.length; l++){
         print[l] += (j % 3 == 0) && (l == 0)?"":"   ";
         print[l] += line[l] +  ((j + 1) % 3 == 0? "\n": "");
       }
      }   
      formatline(print);
    }
    System.out.print("\n");
    temp.set(FIELD_MONTH, calmonth);
   }

   /* A helper method for print_year(). Each line (char length 66),
   * is parsed for unnecessary spaces at the end and appends a 
   * new line after the last char. Entire lines of spaces are 
   * eliminated as well.
   */
   void formatline (String []print){
    for(String k: print){
      int n = 66;
      if(k != print[0]){
       for(;n > 0 && k.substring(n-1,n).equals(" "); n--);
       k = k.substring(0,n) + "\n";
      }
      String spaces = String.format("%66s"," ");
      System.out.print(k.replace(spaces,""));
    }
   }
   /* Helper function for command. Checks for integer bounds
   * and arguement length bounds.
   */
   void checks(String[] args){
    if (args.length > 3){
      misclib.die("usage: jcal [-locale] [[month] year]");
    }
    if(calyear < 1 || calyear > 9999){
      misclib.die("illegal year value: use 1-9999");
    }else if(calmonth < 0 || calmonth > 11){
      misclib.die("illegal month value; use 1-12");
    }
   }

   /* Takes the input arguements and parses them.
   * The values of calmonth, calyear, LOCAL, are 
   * dependend on how many arguements. Input error
   * checking is also accomplished.
   */
   void command(String[] args){
    calmonth = temp.get(FIELD_MONTH);
    calyear = temp.get(FIELD_YEAR);
    yearmonth = true;
    try {
      if(args.length == 2){
       calmonth = Integer.parseInt (args[0]) - 1;
       calyear = Integer.parseInt (args[1]);
      }else if (args.length == 1){
       calyear = Integer.parseInt (args[0]);
       yearmonth = false;
      }else if(args.length == 3){
       if(!args[0].substring(0,1).equals("-")){
         misclib.die("usage: jcal [-locale] [[month] year]");   
       }
       LOCAL = new Locale(args[0].substring(1));
       calmonth = Integer.parseInt(args[1]) - 1;
       calyear = Integer.parseInt(args[2]);
      }
    } catch (NumberFormatException e) {
      misclib.die("Incorrect integer format");
    }
    checks(args);
   }

   /* Constructs a jcal object, and depending on arguement
   * length the correct methods are executed.
   */
   public static void main (String[] args) {
    jcal cal = new jcal();
    cal.command(args);
    if(args.length % 2 == 0){
      int[][] month = cal.make_month(cal.calmonth, cal.calyear);
      cal.print_month(month);
    } else {
      int[][][] year = cal.make_year(cal.calyear); 
      cal.print_year(year);
    }
   }

}

