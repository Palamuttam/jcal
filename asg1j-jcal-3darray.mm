.so Tmac.mm-etc
.if t .Newcentury-fonts
.TITLE CMPS-012B Fall\~2011 Assignment\~1 "Calendar and a 3D Array"
.RCS "$Id: asg1j-jcal-3darray.mm,v 1.31 2013-03-29 20:00:37-07 - - $"
.PWD
.INITR* \n[.F]
.GETPN* TWO_MONTHS Page_TWO_MONTHS
.GETPN* FULL_YEAR Page_FULL_YEAR
.GETST* TWO_MONTHS Figure_TWO_MONTHS
.GETST* FULL_YEAR Figure_FULL_YEAR
.nr HISTORY 0 1
.ds PROMPT bash-\\n+[HISTORY]\[Do]
.H 1 "Overview"
Write a program which displays the calendar for any particular
month or year.
Its output should be the same as the
.V= cal (1)
utility.
Objectives\(::
Unix commands and the command line,
Introduction to Java and its libraries,
.V= gmake .
.P
Before your begin, experiment with
.V= cal
to see what its output looks like.
Your program will be similar,
but with different options.
Note that a single operand is a year and two operands consist of
a month followed by a year.
Also note that
.V= "cal 9 69"
refers not to the month when Unics was created,
but rather to
.I "mensis September DCCCXXII a.u.c." .
.P
Your program is to use the class name
.V= jcal ,
reside in the file
.V= jcal.java ,
make the class file
.V= jcal.class ,
and finally be placed in the executable file
.V= jcal .
Your synopsis will be as follows,
with none of the options listed in the Linux man page\(::
.SH=BVL
.B=LI SYNOPSIS
\f[CB]jcal\f[R]
\|[[\f[I]month\f[R]]
\|\f[I]year\f[R]]
.LE
.H 1 "Implementation Strategy"
How do you go about writing a program that is larger than trivial\(??
It is necessary to begin with a small piece and add to it,
a little piece at a time.
In the end, the entire program will then be written.
Following is a sequence of steps in constructing this program\(::
.ALX 1 ()
.LI
The subdirectory
.V= misc/
contains sample Java programs for you to study before beginning your
program.
The subdirectory
.V= code/
contains the files you should start with for your program.
.LI
As you are programming, you should always be ready to
refer to the reference manual.
For Java, that is at
.VTCODE* 1 http://docs.oracle.com/javase/7/docs/api/
The specific packages you will need are
.V= java.lang
and
.V= java.io .
In addition,
for this assignment,
.V= java.util.GregorianCalendar
and its ancestor
.V= java.util.Calendar .
.LI
Keep in mind that code can get quite complex,
and to keep it simpler,
make sure that no function exceeds about 20\[en]30 lines of code.
Whenever a function gets too large,
break it up into several functions.
Any time you have duplicate code,
extract the duplicated code into a new function and call
that new function from where the duplicate code used to be.
.LI
Look over the sample program
.V= misc/monthcal.java ,
which shows how to use Java's
.V= GregorianCalendar
class to iterate over a month and determine the day of the week
for each day of the month.
It also checks for the end of the month, 
which occurs when adding 1 to the day causes the month to change
and the day to revert to 1.
Run it with various command line arguments.
.LI
Make sure your program does not crash on a
.V= NumberFormatException
when given bad arguments.
Look at 
.V= misc/integerarg.java
to see how to catch the error.
Of course, you should be printing a usage message to
the standard error in the same format as 
.V= cal (1).
.LI
Extract some code from that example and write a method
.V= make_month
that will accept a month and a year as arguments and return some data
structure representing a month that is suitable for easy printing.
This method should not do any printing.
The purpose of this function is to separate the complexity of
calculating the month from its printing,
since you will have to print months side by side in year format.
Start with\(::
.P
.VCODE 1 "int[][] make_month (int month, int year) {
.VCODE 1 "   int[][] month = new int[WEEKS_IN_MONTH][DAYS_IN_WEEK];
.VCODE 1 "   "
\&.\|.\|.
.VCODE 1 }
.LI
A good data structure for a month is
.V= int[][]
and for a year,
.V= int[][][] .
A year is an array of 12 months.
A month is an array of 6 weeks.
A week is an array of 7 days.
A day is an integer.
Look at the full year for 2013 in
Figure\*[Figure_FULL_YEAR] (page \*[Page_FULL_YEAR]),
and at March, 2013, in
Figure \*[Figure_TWO_MONTHS (page \*[Page_TWO_MONTHS])]
to see why you need 6 weeks.
For days not within a month, you can use 0 as the array element.
.LI
Your main program should just print out the result of this then,
and do it only for the current month.
This step is just a testing phase which will be removed later.
Call this function
.V= print_month .
.LI
Add in a method which will analyze the command line arguments
and return a month and a year or print an error message if the
arguments are not good ones.
Your program should work exactly like
.V= cal .
Output always goes to the standard output, 
but errors go the standard error, not the standard output.
See
.V= System.err
and
.V= System.exit .
Print error messages with
.V= misclib.warn
or
.V= misclib.die .
.LI
To verify that your exit status codes are correct, 
use the command
.V= "echo \(Dostatus"
if you use
.V= csh
and
.V= "echo \(Do?"
if you use
.V= bash .
In Java, the function
.V= System.exit
is used to stop the program and return an exit code.
Returning from the
.V= main
function is equivalent to
.V= System.exit(0) .
.LI
The program must have zero, one, or two command line arguments.
Verify that the month, if present, is in the range 1\[en]12,
and that the year, if present, is in the range 1\[en]9999.
Print a message and exit if not.
.LI
Now add in a function to compute calendars for the entire year.
This is just a matter of
.V= make_year
calling
.V= make_month
12 times.
.LI
Write a function
.V= print_year
to print out a year in the year format,
and a function
.V= print_month
to print a single month when the calendar requires that.
Try to avoid duplicating any code.
.LI
Try various arguments and calling sequences,
both valid and invalid.
Check the output from each.
Are the return codes the same as for
.V= cal (1)\(??
What about the error messages to standard error and the
output to standard output\(??
You can use the command
.V= diff (1)
to check the output of your program against the standard Utility.
What about September, 1752\(??
What about years before 1 and after 9999\(??
What about silly months\(??
What about words instead of numbers\(??
.LI
For each of the tests that you run,
compare your output to that produced by
.V= cal .
This can be done with shell commands such as\(::
.DS
.VTCODE* 1 "cal 2013 >cal.2013.out 2>cal.2013.err"
.VTCODE* 1 "jcal 2013 >jcal.2013.out 2>jcal.2013.err"
.VTCODE* 1 "diff cal.2013.out jcal.2013.out"
.DE
The 
.V= diff (1)
command should produce no output.
If it does, then the programs differ.
Your program should produce identical output to that of
.V= cal .
.LE
.DF CB
.SP
.TS
tab(|); |lfCR |.
_
\*[PROMPT] \f[CB]cal 2013\f[P]
                               2013                               
\0
       January               February                 March       
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
       1  2  3  4  5                   1  2                   1  2
 6  7  8  9 10 11 12    3  4  5  6  7  8  9    3  4  5  6  7  8  9
13 14 15 16 17 18 19   10 11 12 13 14 15 16   10 11 12 13 14 15 16
20 21 22 23 24 25 26   17 18 19 20 21 22 23   17 18 19 20 21 22 23
27 28 29 30 31         24 25 26 27 28         24 25 26 27 28 29 30
                                              31
        April                   May                   June        
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
    1  2  3  4  5  6             1  2  3  4                      1
 7  8  9 10 11 12 13    5  6  7  8  9 10 11    2  3  4  5  6  7  8
14 15 16 17 18 19 20   12 13 14 15 16 17 18    9 10 11 12 13 14 15
21 22 23 24 25 26 27   19 20 21 22 23 24 25   16 17 18 19 20 21 22
28 29 30               26 27 28 29 30 31      23 24 25 26 27 28 29
                                              30
        July                  August                September     
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
    1  2  3  4  5  6                1  2  3    1  2  3  4  5  6  7
 7  8  9 10 11 12 13    4  5  6  7  8  9 10    8  9 10 11 12 13 14
14 15 16 17 18 19 20   11 12 13 14 15 16 17   15 16 17 18 19 20 21
21 22 23 24 25 26 27   18 19 20 21 22 23 24   22 23 24 25 26 27 28
28 29 30 31            25 26 27 28 29 30 31   29 30
\0
       October               November               December      
Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa   Su Mo Tu We Th Fr Sa
       1  2  3  4  5                   1  2    1  2  3  4  5  6  7
 6  7  8  9 10 11 12    3  4  5  6  7  8  9    8  9 10 11 12 13 14
13 14 15 16 17 18 19   10 11 12 13 14 15 16   15 16 17 18 19 20 21
20 21 22 23 24 25 26   17 18 19 20 21 22 23   22 23 24 25 26 27 28
27 28 29 30 31         24 25 26 27 28 29 30   29 30 31
\0
\0
\*[PROMPT]
_
.TE
.SP .5v
.FG "\f[B]Output of \f[CB]cal\f[P] for a year\f[P]" "" 0 FULL_YEAR
.SP
.DE
.DF CB
.SP
.TS
tab(|); |lefCR |lw(3m) |lefCR |.
_||_
T{
\*[PROMPT] \f[CB]cal 9 1752\f[P]
   September 1752   
Su Mo Tu We Th Fr Sa
       1  2 14 15 16
17 18 19 20 21 22 23
24 25 26 27 28 29 30
\0
\0
\0
\*[PROMPT]
T}||T{
\*[PROMPT] \f[CB]cal 3 2013\f[P]
     March 2013     
Su Mo Tu We Th Fr Sa
                1  2
 3  4  5  6  7  8  9
10 11 12 13 14 15 16
17 18 19 20 21 22 23
24 25 26 27 28 29 30
31
\*[PROMPT]
T}
_||_
.TE
.SP .5v
.FG "\f[B]A short month and a long month\f[P]" "" 0 TWO_MONTHS
.SP 3v
.DE
.H 1 "The Julian and Gregorian Calendars"
According to legend, the Roman calendar,
was instituted by Romulus in about 753 BCE,
but actually probably deriving from the Greek lunar calendar.
That Roman calendar had 10 months (304 days) per year,
M\[a_]rtius, Apr\[i_]lis, Maius, I\[u_]nius, Qu\[i_]nct\[i_]lis,
Sext\[i_]lis, September, Oct\[o_]ber, Nouember, and December.
plus about 61 days during the winter which were not counted.
King Numa Pompilius, about 713 BCE,
added I\[a_]nu\[a_]rius and Febru\[a_]rius,
making the year 355 days long,
with an intercalary month, Mercedonius, every so often.
In 45 BCE, G\[a_]ius I\[u_]lius Caesar reformed the calendar
to the 365 days we know, plus an extra day every fourth year,
and renamed Qu\[i_]nct\[i_]lis to I\[u_]lius, after himself.
Imper\[a_]tor G\[a_]ius I\[u_]lius Caesar Oct\[a_]ui\[a_]nus Augustus
renamed Sext\[i_]lis to Augustus.
Tiberius refused to have September named after himself,
asking,
``What would happen after Rome has had more then 12 emperors\(??''
.P
The Gregorian calendar was introduced in Italy on
October 15, MDLXXXII.
Astronomer Christopher Glavius
actually did the work,
but Pope Gregory XIII put his name on it.
On September 14, MDCCLII, King George II
adopted the Gregorian calendar for the British Empire.
Prior to that date, 
the Julian calendar was used.
So, for the British Empire and her colonies, 
September, 1752, is a rather strange month, 
as shown in
Figure \*[Figure_TWO_MONTHS] (page \*[Page_TWO_MONTHS]).
.P
The Gregorian calendar has a leap year every 4 years, 
except that years divisible by 100 are not leap years,
but years divisible by 400 are.
Hence 2000 was a leap year, but 1900 was not and 2100 will not be.
The average length of a Gregorian calendar year is thus 365.2425 days,
whereas the actual length of the
tropical year 2000 was 365.242190 days.
The Gregorian calendar is thus inaccurate by one day in 
3225.8 years (26.784 seconds per year).
By contrast, the Julian calendar, with a leap year every 4 years,
was inaccurate by one day in 128.0 years
(674.784 seconds per year = 11.2464 minutes per year).
The current day length is about 86400.003 seconds.
.EQ
delim $$
.EN
And a second is defined such that $ "" sup 133 { roman Cs } $
has an atomic frequency of 9192631770 Hz.
.H 1 "Internationalization (a.k.a. I18N) of the Base Assignment"
The base level assignment should work in exactly the way that
.V= cal (1)
does, including output to
.V= stdout
and
.V= stderr ,
and it should return the same exit status codes.
But it does not handle
.V= cal 's
numerous options.
This part adds additional functionality
by allowing output in multiple different languages.
(The reason the abbreviation is I18N is that there are 18 letters
between the ``i'' and the ``n'' in the word ``internationalization''.)
First study the example program
.V= misc/i18nmonth.java ,
and import the package
.V= java.util.Locale
into your program.
These packages, used as in the example program,
can be used to translate the month and day names into the
appropriate language.
Your new synopsis will be\(::
.SH=BVL
.B=LI SYNOPSIS
\f[CB]jcal\f[R]
\|[\f[B]\-\f[I]locale\f[R]]
\|[[\f[I]month\f[R]]
\|\f[I]year\f[R]]
.LE
.ALX 1 ()
.LI
The
.IR locale
option will be specified as any one of the ISO-639
language names supported by Java.
The sample program 
.V= misc/localeinfo.java
prints them all out, 
but is not part of the assignment.
It just shows you what you might use as the locale argument.
So, for example, with various options, output can be in 
.ds GK' \&\s+2'\h'-2p'\s-2
.if n .ds Greek Ell�nik�
.if t .ds Greek \*[GK']\(*E\(*l\(*l\(*y\(*n\(*i\(*k\(*a\*'
.de lang
.   nop \\$2
.   =V ( \-\\$1 ),
..
.lang da Dansk
.lang en English
.lang es espa�ol
.lang fr fran�ais
.lang ga Gaeilge
.lang it italiano
.lang nl Nederlands
.lang no norsk
.lang pt portugu�s
.lang fi suomi
.lang sv svenska
.lang is �slenska
etc.
Of course, since our
.V= xterm (1)
programs use ISO-8859-1 (ISO-Latin-1) which supports the
langauges of Western Europe, 
those languages that use non-Latin characters will not appear properly.
Unicode supports them all.
.LI
Print out all of the day names as the first two
letters of the name of the day,
and the month names either as the first three letters or as
the complete name in the same way that
.V= cal (1)
does.
If no locale is specified your output must be identical to that of
.V= cal .
The program
.V= misc/printcalnames.java
shows you how to obtain the names of days and months in foreign
languages.
You will only be using the single-argument form of
.V= "new Locale" .
.LI
Another program,
.V= misc/i18nmonth.java
does much the same and is another example.
Note that for languages which do not use the Roman alphabet,
the output is gibberish.
If you have a terminal capable of handling other languages,
you might try
.lang ar Arabic
.lang el \*[Greek]
.lang iw Hebrew
.lang ja Japanese
.lang ru Russian
.lang zh Chinese
etc., but obviously the grader will not do any of that.
.LI
The Julian to Gregorian changeover date varies from country
to country, and you may do a web search to find out a list of
days for each country.
However, since you must emulate
.V= cal (1),
you should hard code the British Empire's changeover date
of September 14, 1752.
.LE
.H 1 "What to Submit"
Look in the subdirectory
.V= \&.score
for instructions to the graders.
The file
.V= SCORE
contains detailed instructions.
The scripts 
.V= mk.build
and
.V= mk.test
will be used to test your program.
You should use these scripts before the graders see your code.
That way you can attempt to grade your program yourself.
.P
.E= "Submit early."
.E= "Submit often."
See lab 1 for more information on how to verify your submit.
If you submit the wrong version of your code or forget to submit
a file, you will lose many points.
Submit the following files\(::
.V= jcal.java
and
.V= misclib.java ,
containing your source code\(;;
.V= Makefile ,
which will build the jar file
.V= jcal
from the source file,
with the target
.V= all
being first and which builds
.V= jcal \(;;
.V= README ,
which should contain your name and username and any specific
things the grader should know.
Did you do the internationalized version\(??
.P
If you are doing pair programming,
carefully read the document in
.V= cmps012b-wm/Syllabus/pair-programming/
and submit the 
.V= PARTNER
file as required.
Pair programming is encouraged in all assignments.
.P
Every file you submit should have as its first line your name
and username.
Its second line should be an RCS
.V= \(DoId\(Do
string in a comment.
.E= CAUTION\(::
Before submitting anything,
carefully read the section in the syllabus that covers cheating.
For instructions to the grader,
see the
.V= \&.score
subdirectory.
.E= "Verify your submit."
.FINISH
