for i in {1..9999}
do
    cal $i > cal.out
    ./jcal $i > jcal.out
    dif="$(diff jcal.out cal.out)"
    if [ "$dif" != "" ]
    then
	echo "jcal $i:" >> output.out
    fi
    diff jcal.out cal.out >> output.out
    for j in {1..12}
    do
	cal $j $i > cal.out
	./jcal $j $i > jcal.out
	fid="$(diff jcal.out cal.out)"
	if [ "$fid" != "" ]
	then
	    echo "jcal $j $i:" >> output.out
	fi 
	diff jcal.out cal.out >> output.out
    done
done
#cal 9 9999 > cal.out
#jcal 9 9999 > jcal.out
#diff cal.out jcal.out